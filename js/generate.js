// 获得按钮
let generate = document.getElementById("generate");
// 绑定事件
generate.addEventListener("click", async () => {
    // 获得值
    let frame = document.getElementById("frame").value;
    let [tab] = await chrome.tabs.query({active: true, currentWindow: true});
    chrome.scripting.executeScript({
        target: {tabId: tab.id},
        function: generateMethod,
        args: [frame]
    });
});

// 生成占位符
function generateMethod(frame) {
    console.log('generateMethod...')
    console.log(frame)
    console.log(document.getElementById(frame).contentWindow.document.body.innerHTML);
    document.getElementById(frame).contentWindow.document.body.innerHTML = '<p>123</p>';
}
