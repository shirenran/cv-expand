let goodWordList = ['春风得意马蹄疾，一日看尽长安花。'
    , '大鹏一日同风起，扶摇直上九万里。'
    , '长风破浪会有时，直挂云帆济沧海。'
    , '好风凭借力，送我上青云。'
    , '手中电击倚天剑，直斩长鲸海水开。'
    , '衣带渐宽终不悔,为伊消得人憔悴。'
    , '众里寻她千百度，蓦然回首，那人却在灯火阑珊处。'
    , '但愿人长久,千里共婵娟。'
    , '山无棱,天地合,乃敢与君绝。'
    , '庄生晓梦迷蝴蝶,望帝春心托杜鹃。'
    , '安得广厦千万间，大庇天下寒士俱欢颜。'
    , '以铜为镜，可正衣冠,以古为镜可见兴替,以人为镜，可知得失。'
    , '莫因善小而不为,莫因恶小而为之。'
    , '天行健君子以自强不息,地势坤君子以厚德载物。'
    , '我住长江头，君住长江尾。日日思君不见君，共饮长江水。'
    , '庭院深深深几许，杨柳堆烟，帘幕无重数。'
    , '昨夜西风凋碧树。独上高楼，望尽天涯路。']
document.getElementById('goodWords').innerText = goodWordList[Math.floor(Math.random() * goodWordList.length)];
// 获得按钮
let my = document.getElementById("my");
// 绑定事件
my.addEventListener("click", async () => {
    // 获得值
    let answer = document.getElementById('answer').value;
    let frame = document.getElementById("frame").value;
    let [tab] = await chrome.tabs.query({active: true, currentWindow: true});
    chrome.scripting.executeScript({
        target: {tabId: tab.id},
        function: setAnswer,
        args: [answer, frame]
    });
});

// 设置答案
function setAnswer(answer, frame) {
    document.getElementById(frame).contentWindow.document.body.innerHTML = '<p>' + answer + '</p>';
}
