// 获得按钮
let getAnswer = document.getElementById("getAnswer");
// 绑定事件
getAnswer.addEventListener("click", async () => {
    let frame = document.getElementById("frame").value;
    console.log('frame123', frame)
    // 获得值
    let [tab] = await chrome.tabs.query({active: true, currentWindow: true});
    chrome.scripting.executeScript(
        {
            target: {tabId: tab.id},
            func: getAnswerMethod,
            args: [frame]
        },
        (result) => {
            console.log('result', result);
            var obj = document.getElementById("currentAnswer");
            obj.innerHTML = result[0].result;
        });
});

// 获得当前文本框中的答案
function getAnswerMethod(frame) {
    console.log('frame', frame);
    let currentAnswer = document.getElementById(frame).contentWindow.document.body.innerHTML;
    return currentAnswer;
}
