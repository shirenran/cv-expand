// 获得按钮
let cleanAnswer = document.getElementById("clean");
// 绑定事件
cleanAnswer.addEventListener("click", async () => {
    // 获得值
    let frame = document.getElementById("frame").value;
    let [tab] = await chrome.tabs.query({active: true, currentWindow: true});
    chrome.scripting.executeScript({
        target: {tabId: tab.id},
        function: cleanAnswerMethod,
        args: [frame]
    });
});

// 清理答题框
function cleanAnswerMethod(frame) {
    console.log('cleanAnswerMethod...')
    document.getElementById(frame).contentWindow.document.body.innerHTML = '';
}
