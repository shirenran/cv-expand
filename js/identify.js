// 获得按钮
let identify = document.getElementById("identify");
// 绑定事件
identify.addEventListener("click", async () => {
    // 获得值
    let [tab] = await chrome.tabs.query({active: true, currentWindow: true});
    chrome.scripting.executeScript(
        {
            target: {tabId: tab.id},
            func: identifyMethod,
        },
        (result) => {
            var obj = document.getElementById("frame");
            obj.options.length = 0;
            let resultList = result[0].result;
            for (let i = 0; i < resultList.length; i++) {
                let num = i + 1;
                obj.add(new Option("第" + num + "个", "ueditor_" + i));
            }
        });
    // chrome.scripting.executeScript({
    //   target: { tabId: tab.id },
    //   function: cleanAnswerMethod,
    // });
});

// 识别富文本数量
function identifyMethod() {
    let j = [];
    for (let i = 0; i < 10; i++) {
        let id = 'ueditor_' + i;
        let elementById = document.getElementById(id);
        if (null === elementById) {
            break;
        }
        j[i] = i;
    }
    return j;
}
